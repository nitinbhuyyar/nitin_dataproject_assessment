const plotlib = require('nodeplotlib');
const csv = require('csv-parser')
const fs = require('fs');
const { getSystemErrorMap } = require('util');
var results = [];
var indiapopulation = [];
var indianyear = []; 
var second = [];
var third = [];
fs.createReadStream('population-estimates_csv.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
   
 
    //console.log(results[0].Year);
    for(var i = 0; i < results.length; i++) {
        if(results[i].Region == 'India')
        {
            indiapopulation.push(results[i].Population) ;
            indianyear.push(results[i].Year);
        }
        if(results[i].Year == 2014)
        {
            if(results[i].Region == 'South-Eastern Asia' ||
                results[i].Region == 'Brunei'||
                results[i].Region == 'Cambodia'||
                results[i].Region == 'Indonesia'||
                results[i].Region == 'Laos'||
                results[i].Region == 'Malaysia'||
                results[i].Region == 'Myanmar'||
                results[i].Region == 'Philippines'||
                results[i].Region == 'Singapore'||
                results[i].Region == 'Thailand'||
                results[i].Region == 'Vietnam'
            )
            {
                second.push(results[i].Population) ;
               // indianyear.push(results[i].Year);
            }


        }
        if(results[i].Region == 'Afghanistan' ||
            results[i].Region == 'Bangladesh'||
            results[i].Region == 'Bhutan'||
            results[i].Region == 'India'||
            results[i].Region == 'Maldives'||
            results[i].Region == 'Nepal'||
            results[i].Region == 'Pakistan'||
            results[i].Region == 'Sri Lanka'
        )
        {
            third.push(results[i])
        }
    }
    const map = new Map();
    for(var i = 0; i < third.length; i++) 
    {
        if(!map.has(third[i].Year))
        {
            map.set(third[i].Year,parseFloat(third[i].Population));
        }
        else{

            map.set(third[i].Year,parseFloat(third[i].Population)+map.get(third[i].Year));

        }


    }
    var years = [];
    var popu = [];
    for (let [key, value] of  map.entries()) {
        years.push(key);
        popu.push(value);
       
    }
    //console.log(map);
    //
    const  data2= [{x:years, y:popu, type: 'bar'}];
    const  data1= [{x:[2014] , y:second, type: 'bar'}];
    const  data= [{x:indianyear , y:  indiapopulation, type: 'bar'}];
    var layout2 = {title:"Total SAARC population vs. year"};
    var layout1 = {title:"population of ASEAN"};
    var layout = {title:"Indian Populations vs years"};
   // plotlib.plot(data,layout);
    //plotlib.plot(data1,layout1);
   // console.log(third);
   plotlib.plot(data2,layout2);

});

//

//'population-estimates_csv.csv'
//South-Eastern Asia 
//Brunei 
//Cambodia
//Indonesia
//Laos
/*
Malaysia
Myanmar
 Philippines
 Singapore
Thailand
Vietnam



Afghanistan
Bangladesh
Bhutan
India
Maldives
Nepal
Pakistan
Sri Lanka

*/