console.log("Inside");


//shell 1
/*
mkdir hello
    cd hello
    mkdir five one
        cd five
        mkdir six
            cd six
            echo '' > c.txt
            mkdir seven
                cd seven
                echo '' > error.log
            
        cd /../../..  # go back to seven and then five then at hello

        cd ./one
        echo 'Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix,
         development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.' > a.txt
        echo '' > b.txt
        mkdir two
            cd two
            echo '' > d.txt
            mkdir three
                cd three
                echo '' > e.txt
                mkdir four
                    cd four
                    echo '' > access.log



cd ../../../../   # come back to hello

#  to delete files with log extension from parent folder
find . -name "*.log" -delete

cd ../../../ # come back to hello


rm -r five # delete directory and its contents

mv one uno #rename one to uno


    cd uno  # go to uno folder
    mv a.txt ./two
        





*/


/*
SHELL 2


cd shell2
    curl -o book https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt

    head -3 book
    tail -10 book
    grep -o -i  "Harry" book | wc -l  # find Number of occurances of Harry in every newlin and pass it wc to count the word


    head -200 book| tail -100   # get the first 200 lines and then get last 100 lines using tail


    grep -o -E '\w+' book | sort -u -f | wc -l   ## count words   u = unique f = ignore case  o = only-matching e = expression

 

*/